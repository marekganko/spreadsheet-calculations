#!/usr/bin/env node

const utils = require('../src/utils')
const { join } = require('path')
const [SOURCE, TARGET] = process.argv.slice(2)
if (!SOURCE.endsWith('xlsx')) {
  console.log(`Wrong file format: ${SOURCE.split('.').pop()}`)
  process.exit(1)
}
const SOURCE_XLSX_FILE = (SOURCE && ((!SOURCE.match(/^\/|~/) && join(process.cwd(), SOURCE)) || SOURCE)) || './data/template-for-testing.xlsx'
const TARGET_JSON_FILE = TARGET || `./results/Result${new Date().valueOf()}.json`

try {
  utils.xlsxToJson(SOURCE_XLSX_FILE, TARGET_JSON_FILE)
} catch (e) {
  console.log(e)
}
