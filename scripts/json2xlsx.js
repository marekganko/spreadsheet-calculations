#!/usr/bin/env node

const utils = require('../src/utils')
const { join } = require('path')
const [SOURCE, TARGET] = process.argv.slice(2)
if (!SOURCE.endsWith('json')) {
  console.log(`Wrong file format: ${SOURCE.split('.').pop()}`)
  process.exit(1)
}
const SOURCE_FILE = (SOURCE && ((!SOURCE.match(/^\/|~/) && join(process.cwd(), SOURCE)) || SOURCE)) || './data/template-for-testing.json'
const TARGET_FILE = TARGET || `./results/Result${new Date().valueOf()}.xlsx`

try {
  console.log(SOURCE_FILE, TARGET_FILE)
  utils.jsonToXlsx(SOURCE_FILE, TARGET_FILE)
} catch (e) {
  console.log(e)
}
