const Benchmark = require('benchmark')
const { createWorkbook, feedWorkbookWithDataFromArray, getWorkbookData, clearResultsSheet } = require('../src/xlsx')
const inputData = require('../data/data-source-10.json')
const inputData100 = require('../data/data-source-100.json')
const inputData1000 = require('../data/data-source-1000.json')
const excelJsonTemplateSmall = require('../data/excel-template-with-small-result-table.json')
const excelJsonTemplateBig = require('../data/excel-template-with-big-result-table.json')

const suite = new Benchmark.Suite()
const resultSheetName = 'Calculation'
const resultTableName = '_RESULT'
const SHOW_EACH_RUN_TIME = process.env.SHOW_EACH_RUN_TIME || false

suite
  .add('Input 10 records to small result table', function () {
    if (SHOW_EACH_RUN_TIME) console.time('run')
    try {
      let workbook = createWorkbook(excelJsonTemplateSmall)

      feedWorkbookWithDataFromArray(workbook, inputData)
      getWorkbookData(workbook, resultSheetName, resultTableName)
      workbook = null
    } catch (e) {
      console.log(e)
    }
    if (SHOW_EACH_RUN_TIME) console.timeEnd('run')
  })
  .add('Input 10 records to big result table', function () {
    if (SHOW_EACH_RUN_TIME) console.time('run')
    try {
      let workbook = createWorkbook(excelJsonTemplateBig)
      const numberOfRows = inputData[1].data.length

      feedWorkbookWithDataFromArray(workbook, inputData)
      getWorkbookData(workbook, resultSheetName, resultTableName)
      workbook = null
    } catch (e) {
      console.log(e)
    }
    if (SHOW_EACH_RUN_TIME) console.timeEnd('run')
  })
  .add('Input 10 records to big result table after cleaning rows', function () {
    if (SHOW_EACH_RUN_TIME) console.time('run')

    try {
      let workbook = createWorkbook(excelJsonTemplateBig)
      const numberOfRows = inputData[1].data.length

      clearResultsSheet(workbook, resultSheetName, resultTableName, numberOfRows)
      feedWorkbookWithDataFromArray(workbook, inputData)
      getWorkbookData(workbook, resultSheetName, resultTableName)
      workbook = null
    } catch (e) {
      console.log(e)
    }
    if (SHOW_EACH_RUN_TIME) console.timeEnd('run')
  })
  .add('Input 10 records repeated 10 times ', function () {
    if (SHOW_EACH_RUN_TIME) console.time('run')

    Array(10)
      .fill()
      .map((_, index) => {
        if (SHOW_EACH_RUN_TIME) console.time(`  · ${index}/10`)
        let workbook = createWorkbook(excelJsonTemplateBig)
        const numberOfRows = inputData[1].data.length

        clearResultsSheet(workbook, resultSheetName, resultTableName, numberOfRows)
        feedWorkbookWithDataFromArray(workbook, inputData)
        getWorkbookData(workbook, resultSheetName, resultTableName)
        workbook = null
        if (SHOW_EACH_RUN_TIME) console.timeEnd(`  · ${index}/10`)
      })

    if (SHOW_EACH_RUN_TIME) console.timeEnd('run')
  })
  .add('Input 100 records to big result table', function () {
    if (SHOW_EACH_RUN_TIME) console.time('run')
    try {
      let workbook = createWorkbook(excelJsonTemplateBig)
      const numberOfRows = inputData100[1].data.length

      feedWorkbookWithDataFromArray(workbook, inputData100)
      getWorkbookData(workbook, resultSheetName, resultTableName)
      workbook = null
    } catch (e) {
      console.log(e)
    }
    if (SHOW_EACH_RUN_TIME) console.timeEnd('run')
  })
  .add('Input 100 records to big result table after cleaning rows', function () {
    if (SHOW_EACH_RUN_TIME) console.time('run')
    try {
      let workbook = createWorkbook(excelJsonTemplateBig)
      const numberOfRows = inputData100[1].data.length

      clearResultsSheet(workbook, resultSheetName, resultTableName, numberOfRows)
      feedWorkbookWithDataFromArray(workbook, inputData100)
      getWorkbookData(workbook, resultSheetName, resultTableName)
      workbook = null
    } catch (e) {
      console.log(e)
    }
    if (SHOW_EACH_RUN_TIME) console.timeEnd('run')
  })
  .add('Input 1000 records to big result table', function () {
    if (SHOW_EACH_RUN_TIME) console.time('run')
    try {
      let workbook = createWorkbook(excelJsonTemplateBig)
      const numberOfRows = inputData1000[1].data.length

      feedWorkbookWithDataFromArray(workbook, inputData1000)
      getWorkbookData(workbook, resultSheetName, resultTableName)
      workbook = null
    } catch (e) {
      console.log(e)
    }
    if (SHOW_EACH_RUN_TIME) console.timeEnd('run')
  })
  .add('Input 1000 records to big result table after cleaning rows', function () {
    if (SHOW_EACH_RUN_TIME) console.time('run')
    try {
      let workbook = createWorkbook(excelJsonTemplateBig)
      const numberOfRows = inputData1000[1].data.length

      clearResultsSheet(workbook, resultSheetName, resultTableName, numberOfRows)
      feedWorkbookWithDataFromArray(workbook, inputData1000)
      getWorkbookData(workbook, resultSheetName, resultTableName)
      workbook = null
    } catch (e) {
      console.log(e)
    }
    if (SHOW_EACH_RUN_TIME) console.timeEnd('run')
  })
  .on('cycle', function (event) {
    console.log(String(event.target))
  })
  .run()
