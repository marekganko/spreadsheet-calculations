# Spreadsheet Calculations (PoC)

### Prerequisites

- Node v12.x.x
- Yarn

[Linux / Mac installation](https://github.com/nvm-sh/nvm#install--update-script)

[Windows installation](https://docs.microsoft.com/en-us/windows/nodejs/setup-on-windows#alternative-version-managers)

To easily switch between different versions run command below which automatically detects a required version

```bash
$ nvm use
```

## Description

Chceking possibilities of SpreadJS library used with nodejs

[Project page](https://www.grapecity.com/spreadjs)

[Documentation](https://www.grapecity.com/spreadjs/docs/v13/online/overview.html)

## Installation

```bash
$ yarn
```

### Create license file to put valid license key

```
cp config/license.example config/license
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

# parse excel file to json (parsed json file by default lands in ./results/ directory)
$ yarn xlsx2json path/to/file.xlsx [ path/to/result.json ]

# parse json file to excel (parsed excel file by default lands in ./results/ directory)
$ yarn json2xlsx path/to/file.json [ path/to/result.xlsx ]

# run benchmarks
$ yarn start:bench

```

## Results from benchmark

Tests performed with Nodejs v12 on MacBook Pro (16-inch, 2019) with 2,4 GHz 8-Core i9 and 32GB DDR4 RAM

```bash
Input 10 records to small result table x 16.74 ops/sec ±11.84% (41 runs sampled)
Input 10 records to big result table x 0.56 ops/sec ±3.42% (6 runs sampled)
Input 10 records to big result table after cleaning rows x 1.48 ops/sec ±3.13% (8 runs sampled)
Input 100 records to big result table x 0.59 ops/sec ±2.14% (6 runs sampled)
Input 100 records to big result table after cleaning rows x 1.09 ops/sec ±8.75% (7 runs sampled)
Input 1000 records to big result table x 0.16 ops/sec ±2.83% (5 runs sampled)
Input 1000 records to big result table after cleaning rows x 0.13 ops/sec ±1.13% (5 runs sampled)
```

_cleaning rows removing all existing rows from the excel \_RESULT table_
