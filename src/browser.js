const { JSDOM } = require('jsdom')
const fileReader = require('filereader')
module.exports = () => {
  const dom = new JSDOM()
  global.window = dom.window
  global.document = window.document
  global.navigator = window.navigator
  global.HTMLCollection = window.HTMLCollection
  global.getComputedStyle = window.getComputedStyle
  global.FileReader = fileReader
}
