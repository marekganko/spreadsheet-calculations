require('./browser')()
const { GC, GCExcel } = require('./spread')
const fs = require('fs')

const createWorkbook = (excelJsonTemplate) => {
  const workbook = new GC.Spread.Sheets.Workbook()

  workbook.fromJSON(excelJsonTemplate)

  return workbook
}

const formatCells = (tableRange, cellFormats, activeSheet) => {
  const { rowCount, colCount, row, col } = tableRange

  Array(rowCount)
    .fill()
    .map((_, rowIndex) =>
      Array(colCount)
        .fill()
        .map((_, colIndex) => activeSheet.setFormatter(row, col, cellFormats[colIndex - col])),
    )
}

const fillSheetWithData = (activeSheet, table, data, cellFormats) => {
  const tableRange = table.range()
  const { rowCount, row, col } = tableRange
  const realFirstRow = row + 1

  // Important, adjust the activeSheet rows, so that it can cover all input rows
  activeSheet.setRowCount(realFirstRow + data.length)

  if (rowCount > data.length) {
    // Delete not needed rows and set data, no need for cell formatting
    table.deleteRows(0, rowCount - data.length)
    activeSheet.setArray(realFirstRow, col, data, false)
  } else if (rowCount < data.length) {
    // We need to enlarge the table first, so that the data will fit in
    table.insertRows(0, data.length - rowCount)
    activeSheet.setArray(realFirstRow, col, data, false)
    // Apply proper cell format to the additional cells
    // This is not needed for server side calculations, as we are only interested in the raw result values, but it works:
    if (process.env.NODE_ENV !== 'test') formatCells(tableRange, cellFormats, activeSheet)
  } else {
    // no table adjustment needed, just set the data
    activeSheet.setArray(realFirstRow, col, data, false)
  }
}

const feedWorkbookWithDataFromArray = (workbook, inputData) => {
  inputData.map(({ sheetName, tableName, data }) => {
    activeSheet = workbook.getSheetFromName(sheetName)
    const table = activeSheet && activeSheet.tables.findByName(tableName)
    if (table) {
      // get current range of table
      const { colCount, row } = table.range()

      // get cell formats, so that we can apply them later if needed
      const cellFormats = Array(colCount)
        .fill()
        .map((_, colIndex) => activeSheet.getFormatter(row, colIndex))

      activeSheet.suspendPaint()
      fillSheetWithData(activeSheet, table, data, cellFormats)
      activeSheet.resumePaint()
    }
  })
}

const clearResultsSheet = (workbook, sheetName, tableName, numberOfRows) => {
  const activeSheet = workbook.getSheetFromName(sheetName)
  const table = activeSheet && activeSheet.tables.findByName(tableName)
  activeSheet.setRowCount(2 + numberOfRows)
  table.deleteRows(numberOfRows + 2, 1000)
  return workbook
}

const getWorkbookData = (workbook, sheetName, tableName) => {
  const activeSheet = workbook.getSheetFromName(sheetName)
  const table = activeSheet && activeSheet.tables.findByName(tableName)

  if (!table) {
    throw new Error(`Table ${tableName} doesn't exist in sheet ${sheetName}`)
  }

  const { rowCount, colCount, row } = table.range()
  const columnName = (colIndex) => activeSheet.getCell(row, colIndex).value()
  const cellValue = (rowIndex, colIndex) => activeSheet.getCell(rowIndex, colIndex).value()

  const result = Array(rowCount)
    .fill()
    .map((_, rowIndex) => {
      return Array(colCount)
        .fill()
        .map((_, colIndex) => {
          const name = columnName(colIndex)
          const value = cellValue(rowIndex + 2, colIndex)
          return { [name]: value }
        })
    })
  return result
}

const exportWorkbookToExcel = (workbook, fileName = `./results/Result${new Date().valueOf()}.xlsx`) => {
  const excelIO = new GCExcel.IO()
  const workbookJson = workbook.toJSON()

  excelIO.save(
    workbookJson,
    (data) => {
      fs.writeFileSync(fileName, Buffer.from(data), function (err) {
        console.log(err)
      })
      console.log(`Excel file exported to ${fileName}`)
    },
    (err) => console.log(err),
    { useArrayBuffer: true },
  )
}

module.exports = {
  createWorkbook,
  exportWorkbookToExcel,
  getWorkbookData,
  feedWorkbookWithDataFromArray,
  clearResultsSheet,
}
