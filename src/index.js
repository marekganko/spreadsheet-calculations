const { createWorkbook, feedWorkbookWithDataFromArray, exportWorkbookToExcel, getWorkbookData, clearResultsSheet } = require('./xlsx')
const inputData = require('../data/data-source-10.json')
const excelJsonTemplate = require('../data/template_from_neo_backend.json')
const resultSheetName = 'Calculation'
const resultTableName = '_RESULT'
function simplifyInputDataStructure(inputData) {
  return inputData.map(({ name, data }) => ({ name, data: data.map((value) => Object.values(value)) }))
}

try {
  let workbook = createWorkbook(excelJsonTemplate)
  const numberOfRows = inputData[1].data.length

  workbook = clearResultsSheet(workbook, resultSheetName, resultTableName, numberOfRows)
  feedWorkbookWithDataFromArray(workbook, simplifyInputDataStructure(inputData))
  const result = getWorkbookData(workbook, resultSheetName, resultTableName)
  const json = JSON.stringify(result)
  exportWorkbookToExcel(workbook)
} catch (e) {
  console.log(e)
}
