const fs = require('fs')
require('./browser')()
const { GCExcel, GC } = require('./spread')
const { exportWorkbookToExcel } = require('./xlsx')

const xlsxToJson = (srcFilePath, targetFilePath) => {
  const excelIO = new GCExcel.IO()
  const workbook = new GC.Spread.Sheets.Workbook()
  const file = fs.readFileSync(srcFilePath)

  excelIO.open(file.buffer, (excelJson) => {
    workbook.fromJSON(excelJson)
    const workbookJson = workbook.toJSON()
    fs.writeFileSync(targetFilePath, Buffer.from(JSON.stringify(workbookJson, null, 2)), (e) => {
      throw e
    })
    console.log(`JSON file exported to ${targetFilePath}`)
  })
}

const jsonToXlsx = (srcFilePath, targetFilePath) => {
  const workbook = new GC.Spread.Sheets.Workbook()
  const file = fs.readFileSync(srcFilePath)
  workbook.fromJSON(JSON.parse(file))
  exportWorkbookToExcel(workbook, targetFilePath)
}

module.exports = {
  xlsxToJson,
  jsonToXlsx,
}
