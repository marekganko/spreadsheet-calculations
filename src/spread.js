const fs = require('fs')
const { join } = require('path')
const GC = require('@grapecity/spread-sheets')
const GCExcel = require('@grapecity/spread-excelio')

const filePath = join(__dirname, '../config/license')
const license = fs.readFileSync(filePath, 'utf8')

GC.Spread.Sheets.LicenseKey = GCExcel.LicenseKey = license

module.exports = {
  GC,
  GCExcel,
}
